﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MyFirst2DPlatform.Mechanics
{
    [ExecuteInEditMode()]
    public class UILifeBar : MonoBehaviour
    {
        Image _life1;
        Image _life2;
        Image _life3;

        Color32 ACTIVE_COLOR = new Color32(225, 225, 225, 225);
        Color32 INTACT_COLOR = new Color32(0, 0, 0, 225);

        public void SetLifes(int lifes)
        {
            switch (lifes)
            {
                case 0:
                    _life1.color = INTACT_COLOR;
                    _life2.color = INTACT_COLOR;
                    _life3.color = INTACT_COLOR;
                    break;

                case 1:
                    _life1.color = ACTIVE_COLOR;
                    _life2.color = INTACT_COLOR;
                    _life3.color = INTACT_COLOR;
                    break;

                case 2:
                    _life1.color = ACTIVE_COLOR;
                    _life2.color = ACTIVE_COLOR;
                    _life3.color = INTACT_COLOR;
                    break;

                case 3:
                    _life1.color = ACTIVE_COLOR;
                    _life2.color = ACTIVE_COLOR;
                    _life3.color = ACTIVE_COLOR;
                    break;

            }


        }

        private void Awake()
        {
            _life1 = transform.Find("Life1").GetComponent<Image>();
            _life2 = transform.Find("Life2").GetComponent<Image>();
            _life3 = transform.Find("Life3").GetComponent<Image>();
        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
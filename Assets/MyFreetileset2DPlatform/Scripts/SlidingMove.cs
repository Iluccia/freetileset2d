﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace MyFirst2DPlatform.Mechanics
{
    [RequireComponent(typeof(SliderJoint2D))]

    public class SlidingMove : MonoBehaviour
    {
        //Transform _connectedObjectTransform;

        [SerializeField]float MaxDisplacement = 4;

        SliderJoint2D _slinderJoint;

        State _state;

        enum State
        {
            Stopped,
            Moving,
            InvertingDirection
        }

        private void Awake()
        {
            _slinderJoint = GetComponent<SliderJoint2D>();
        }
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (_state != State.InvertingDirection)
            {
                if (_slinderJoint.motor.motorSpeed == 0)
                    _state = State.Stopped;

                else
                    _state = State.Moving;
            }
        }

        void FixedUpdate()
        {

            //Debug.Log("_slinderJoint.jointTranslation" + _slinderJoint.jointTranslation);

            if (_state == State.InvertingDirection)
            {
                if (MaxDisplacement - Mathf.Abs(_slinderJoint.jointTranslation) > float.Epsilon)

                    _state = State.Moving;
            }

            else
                if (Mathf.Abs(_slinderJoint.jointTranslation) >= MaxDisplacement)
                InvertSlideDirection();

        }

        private void InvertSlideDirection()
        {
            _state = State.InvertingDirection;

            JointMotor2D motor = _slinderJoint.motor;
            motor.motorSpeed *= -1;
            _slinderJoint.motor = motor;

        }

    }
}
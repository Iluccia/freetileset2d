﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MyFirst2DPlatform.Mechanics
{
    public class ZombiEnemi : MonoBehaviour
    {
        //[SerializeField] GameObject target;

        [SerializeField] int _lifePoints;
        [SerializeField] bool FlipOnStart;

        [SerializeField] int _attackPoints = 1;
        [SerializeField] float _waitSecondsForNextAttack = 2;
        [SerializeField] float _viewDistance = 2;
        [SerializeField] float _viewRadius = 1;
        [SerializeField] private float _timeAfterDeath = 10;
        [SerializeField] float _attackDinstance = 0.5f;
        [SerializeField] private float _TimeAfterDeath = 10;
        [SerializeField] float _walkForce = 1;

        CapsuleCollider2D _bodyCollider;
        Animator _animator;
        SpriteRenderer _spriteRenderer;
        Coroutine _currentRoutine;
        GameObject _target;
        Rigidbody2D _rigidBody;

        bool _doAttack = true;
        bool m_FacingRight = true;
        bool _isDead;
        private bool FlipOnStar;

        private Animator m_Anima;
        private bool _isFacingRight;

        private void Awake()
        {
            _animator = GetComponent<Animator>();
            _spriteRenderer = GetComponent<SpriteRenderer>();
            _bodyCollider = GetComponent<CapsuleCollider2D>();
        }

        public Vector3 FacingDirection
        {
            get
            {
                return (_isFacingRight) ? transform.right : -transform.right;

            }

        }

        // Use this for initialization
        void Start()
        {
            //Debug.Log("lifepoints " + _lifePoints);
            if (FlipOnStar)
                Flip();

            _currentRoutine = StartCoroutine(IdleLogic());
        }

        // Update is called once per frame
        void Update()
        {

        }

        private void OnGUI()
        {

            if (GUILayout.Button("Hit"))
            {
                gameObject.SendMessage("__TAKEDAMAGE__");
            }
            if (GUILayout.Button("ATTACK"))
            {
                Attack();

            }

        }

        private void Flip()
        {
            // Switch the way the player is labelled as facing.
            m_FacingRight = !m_FacingRight;

            // Multiply the player's x local scale by -1.
            Vector3 theScale = transform.localScale;
            theScale.x *= -1;
            transform.localScale = theScale;

        }

        void Attack()
        {
            _animator.SetTrigger("attack");

        }

        //public void __TAKED_AMAGE__(DamageInfo damageInfo)
        //{

        //    LifePoints -= damageInfo.DamagePoint;
        //    if (LifePoints > 0)
        //        m_Anima.SetTrigger("hit");
        //    else
        //    {
        //        if (!isdead)
        //        {
        //            StopCoroutine(_currentRoutine);
        //            m_Anima.SetTrigger("dead");
        //            isdead = true;
        //        }
        //    }
        //}

        public void __TAKE_DAMEGE__(DamageInfo damageInfo)
        {

            if (!_isDead)
            {

                Debug.Log("OUCH!");

                _lifePoints -= damageInfo.DamagePoint;

                if (_lifePoints > 0)
                    _animator.SetTrigger("hit");

                else
                {
                    SwitchRoutine(DeathLogic());

                }

            }

        }

        public void __SHOOT__()
        {
            int HIT_ARREY_LENGTH = 10;
            RaycastHit2D[] hits = new RaycastHit2D[HIT_ARREY_LENGTH];

            Vector3 direction = (_isFacingRight) ? transform.right : -transform.right;

            int layerFliter = 1 << LayerMask.NameToLayer("Characters");

            int hitCount = Physics2D.RaycastNonAlloc(transform.position, direction, hits, _attackDinstance, layerFliter);

            Debug.DrawLine(transform.position, transform.position + direction * _attackDinstance, Color.red);

            if (hitCount > 0)
            {
                Debug.Log("hits" + hitCount);

                for (int i = 0; i < hitCount; i++)
                {
                    RaycastHit2D rh = hits[i];
                    GameObject hitGo = rh.collider.gameObject;

                    Debug.Log(">" + hitGo.name);

                    if (hitGo.GetInstanceID() != gameObject.GetInstanceID() && hitGo.tag == "Player")
                    {
                        DamageInfo damage = new DamageInfo();
                        damage.DamagePoint = _attackPoints;
                        hitGo.SendMessage("__TAKE_DAMAGE__", damage);

                    }

                }

            }

        }

        bool IsFacingTarghet()
        {
            return ((_target.transform.position.x < gameObject.transform.position.x && !_isFacingRight)
                    ||
                    (_target.transform.position.x > gameObject.transform.position.x && _isFacingRight)) ;

        }

        float GetDistanceFromTarghet()
        {
            Vector3 distanceVector = _target.transform.position - transform.position;
            return distanceVector.magnitude;


        }

        bool CanSeePlayer(out List<GameObject> targets)
        {
            bool result = false;
            targets = null;

            int layerFlirter = 1 << LayerMask.NameToLayer("Characters");

            Vector2 origin = transform.position;
            Vector2 direction = transform.position + ((m_FacingRight) ? transform.right : -transform.right) * _viewDistance;
            int HIT_ARRAY_LENGHT = 10;
            RaycastHit2D[] hits = new RaycastHit2D[HIT_ARRAY_LENGHT];
            int hitCount = Physics2D.CircleCastNonAlloc(origin, _viewRadius, direction, hits, _viewDistance);

            if (hitCount > 0)
            {
                targets = new List<GameObject>();

                for (int i = 0; i < hitCount; i++)
                {
                    RaycastHit2D rh = hits[i];
                    GameObject hitGo = rh.collider.gameObject;

                    if (hitGo.tag == "Player")
                    {
                        _target = hitGo;

                        targets.Add(hitGo);
                        //Debug.Log("@#!$%$=?!^");
                        result = true;
                    }

                }

            }

            return result;
        }

        IEnumerator IdleLogic()
        {
            while (true)
            {
                List<GameObject> targets = null;

                if (CanSeePlayer(out targets))
                {
                    if (GetDistanceFromTarghet() > _attackDinstance)
                        SwitchRoutine(walk2TarghetLogic());

                    else
                        SwitchRoutine(AttackLogic());

                }

                yield return new WaitForSeconds(1);

            }

        }

        IEnumerator walk2TarghetLogic()
        {

            Vector3 movementDirection;

            while (GetDistanceFromTarghet() > _attackDinstance)
            {
                List<GameObject> targets = null;
                if (CanSeePlayer(out targets))
                {
                    if (IsFacingTarghet())

                        Flip();

                    movementDirection = this.FacingDirection;

                    _animator.SetFloat("Speed", 1);
                    //transform.Translate(movementDirection * _walkForce * Time.deltaTime);
                    _rigidBody.AddForce(movementDirection * _walkForce * Time.deltaTime);

                }
                else
                {
                    SwitchRoutine(IdleLogic());

                }

                yield return null;
            }

            _animator.SetFloat("Speed", 0);
            SwitchRoutine(AttackLogic());

        }

        IEnumerator AttackLogic()
        {
            _doAttack = true;

            while (_doAttack)
            {
                if (!IsFacingTarghet())
                    Flip();

                if (GetDistanceFromTarghet() > _attackDinstance)
                    SwitchRoutine(walk2TarghetLogic());

                else
                    Attack();

                yield return new WaitForSeconds(_waitSecondsForNextAttack);

            }

        }

        IEnumerator DeathLogic()
        {
            _rigidBody.gr
            _animator.SetTrigger("dead");
            _isDead = true;
            _bodyCollider.enabled = false;
            yield return new WaitForSeconds(_timeAfterDeath);
            Destroy(gameObject);
        }

        void SwitchRoutine(IEnumerator routine)
        {
            if (_currentRoutine != null)
            {
                StopCoroutine(_currentRoutine);

            }
            _currentRoutine = StartCoroutine(routine);
        }
    }
}



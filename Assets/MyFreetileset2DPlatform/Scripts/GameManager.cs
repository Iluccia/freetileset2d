﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace MyFirst2DPlatform.Mechanics
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] GameObject _gameOverPanel;
        [SerializeField] Text _coinCounterText;
        [SerializeField] AudioClip _gameOverAudioClip;
        [SerializeField] GameObject _lifePanel;
        [SerializeField] UiHeartBar _healthBar;
        [SerializeField] Transform StartCheckPoint;
        [SerializeField] Transform CheckPoint;
        [SerializeField] float _playerRespawnTime = 1.5f;

        UILifeBar _lifeBar;
        Guybrush _guybrush;
        GameObject _playerGameObject;
        AudioSource _audioSource;

        private void Awake()
        {
            _audioSource = gameObject.GetComponent<AudioSource>();

        }

        // Use this for initialization
        void Start()
        {
            _gameOverPanel.SetActive(false);
            _playerGameObject = GameObject.FindGameObjectWithTag("Player");
            _guybrush = _playerGameObject.GetComponent<Guybrush>();

            if (_lifePanel == null)
                _lifePanel = GameObject.Find("LifePanel");
            _lifeBar = _lifePanel.GetComponent<UILifeBar>();

            if (_healthBar == null)
                _healthBar = GameObject.Find("HealthBar").GetComponent<UiHeartBar>();
            _healthBar.Value = _guybrush.GetHealth();
            _healthBar.SetMaxValue(_guybrush.GetMaxHealth());

            OnGameStart();
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void OnGameStart() ///Gestione della sequenza di avviso del gioco.
        {
            _playerGameObject.SetActive(true);
            CheckPoint = StartCheckPoint;
            RelocatePlayer(StartCheckPoint.position);

        }

        public void GameOver()///gestione seguenza di fine gioco.
        {
            Debug.Log("GameOver!!!");

            _gameOverPanel.SetActive(true);

            if (!_audioSource.isPlaying)
            {
                _audioSource.PlayOneShot(_gameOverAudioClip);

            }

        }

        public void OnCharacterTakeDamage() /// Gestisce l.evento quando il personaggio riceve il danno.
        {

            _healthBar.Value = _guybrush.GetHealth();

        }

        public void OnCharacterLostLife() /// Gestisce l'evento quando il personaggio perde una vita.
        {
            _lifeBar.SetLifes(_guybrush.GetLifes());
            StartCoroutine(CharacterDeath());
        }

        public void OnCharacterHealthChange()
        {
            _healthBar.Value = _guybrush.GetHealth(); /// gestisce l'evento quando la barra dell'energia cambia.

        }

        public void OnCharacterGetCheckPoint(GameObject checkpoint) /// metodo che viene richiamato quando viene sollevato l'evento CheckPointEvent.
        {
            StartCheckPoint = checkpoint.transform;
        }

        private IEnumerator CharacterDeath()
        {
            yield return new WaitForSeconds(_playerRespawnTime);

            if (_guybrush.GetLifes() > 0)
            {
                _guybrush.SendMessage("__RESPAWN__");

                RelocatePlayer(CheckPoint.position);

            }

        }

        private void RelocatePlayer(Vector3 position)
        {
            _playerGameObject.transform.position = position;

        }
    }
}
﻿using UnityEngine;
using UnityEngine.UI;


public class UiHeartBar : MonoBehaviour
{
    [SerializeField] Image ValueIndicator;
    [SerializeField] int MaxValue;
    [SerializeField] public int Value;

    int _unitValueWidth;
    int _value = int.MinValue;

    public void SetMaxValue(int maxValue)
    {



    }

    // Use this for initialization
    void Start ()
    {
        UpdateUnitSize();
	}
	
	// Update is called once per frame
	void Update ()
    {
	if(_value != Value)
        {
            OnValueChanged();
        }

	}

    void UpdateUnitSize()
    {
        if (MaxValue != 0)
            _unitValueWidth = (int)ValueIndicator.rectTransform.rect.width / MaxValue;

    }

    void OnValueChanged() /// Ridimensiona la barra dell'energia in base ai punti vita del personaggio.
    {
        _value = Value;

        Vector2 sizeDelta = ValueIndicator.rectTransform.sizeDelta;

        sizeDelta.x = _unitValueWidth * _value;

        ValueIndicator.rectTransform.sizeDelta = sizeDelta;

    }



}

﻿using System;

    [Serializable]
public class CharacterData
{
    public int Health = 10;
    public int AttackPoint = 1;
    public float _attackDinstance = 1.0f;
    public int Lifes = 3;
    public int MaxHealth = 10;
    internal float AttackDistance;
}

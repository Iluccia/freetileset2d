﻿using System;
using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

namespace MyFirst2DPlatform.Mechanics
{
    [Serializable]
    public class CheckPointEvent : UnityEvent<GameObject> // classe per evento 
    {


    }

    public struct DamageInfo  // struttura per contenere le informazioni sul danno.
    {
        public int DamagePoint; // punti energia da sottrarre quando viene applicato il danno.

        public bool InstaKill;  // Indica chi il personaggio deve prendere istantaneamente una vita 
                                // indipendentemente dai punti energia che ha.
    }

    public class Guybrush : MonoBehaviour
    {
        [SerializeField] CheckPointEvent _checkPointEvent;
        [SerializeField] UnityEvent _lostLifeEvent;
        [SerializeField] UnityEvent _heartChangedEvent;
        [SerializeField] UnityEvent _gameOverEvent;
        [SerializeField] CharacterData _instanceData;

        [SerializeField] int _lifePoints;
        [SerializeField] float _attackDistance;

        MyPlatformerCharacter2D _character;
        EnhancedPlatformer2DUserControl _input;
        GameObject _interactable;
        CapsuleCollider2D _bodyCollider;

        bool _isDead;
        bool MechPonteLev = false;
        public bool IsMechPonteLev { get { return MechPonteLev; } }
        private bool _canMove;
        private object _platformCollision;

        private Vector2 m_platformVelocity;

        private void Awake()
        {
            _bodyCollider = GetComponent<CapsuleCollider2D>();
            _input = GetComponent<EnhancedPlatformer2DUserControl>();
            _character = GetComponent<MyPlatformerCharacter2D>();
        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (_interactable != null && Input.GetKeyDown(KeyCode.E))
                _interactable.SendMessage("__INTERACT__");

            if (_input.IsAttacking)
            {
                CastAttack();

            }
        }

        private void FixedUpdate()
        {
            if (CanMove())
            {
                _character.Move(_input.Horizontal, _input.IsCrouch, _input.IsJumpingUp, _input.IsJumpingDown, _input.IsAttacking, m_platformVelocity);
                _input.IsJumpingUp = false;
                m_platformVelocity = Vector2.zero;
            }
        }

        private bool CanMove()  //indica se il personaggio può essere mosso dal giocatore.
        {
            if (_character.IsPlayingState("Hit") || _character.IsPlayingState("Dead") || _character.IsDead())
                _canMove = false;

            else
                _canMove = true;

            return _canMove;

        }

        void CastAttack()
        {
            //Debug.Log("ATTACK!");
            int HIT_ARRAY_LENGTH = 20;
            RaycastHit2D[] hits = new RaycastHit2D[HIT_ARRAY_LENGTH];

            Vector3 direction = (_character.IsFacingRight) ? transform.right : -transform.right;

            int layerFilter = 1 << LayerMask.NameToLayer("Characters");

            int hitCount = Physics2D.RaycastNonAlloc(transform.position, direction, hits, _instanceData.AttackDistance, layerFilter);

            //int hitCount = Physics2D.RaycastNonAlloc(transform.position, transform.position + ((_character.IsFacingRight) ? transform.right : -transform.right), hits, _instanceData._attackDinstance);
            //Debug.DrawLine(transform.position, transform.position + ((_character.IsFacingRight) ? transform.right : -transform.right) * _instanceData._attackDinstance, Color.red);

            if (hitCount > 0)
            {
                //Debug.Log("hit" + hitCount);

                for (int i = 0; i < hitCount; i++)
                {
                    RaycastHit2D rh = hits[i];
                    GameObject hitGo = rh.collider.gameObject;
                    //Debug.Log(">" + hitGo.name);

                    if (hitGo.GetInstanceID() != gameObject.GetInstanceID() && hitGo.tag == "enemy")
                    {
                        Debug.Log("HIT!!!");

                        DamageInfo damege = new DamageInfo();
                        damege.DamagePoint = _instanceData.AttackPoint;
                        hitGo.SendMessage("__TAKE_DAMEGE__", damege);

                    }

                }

            }

        }

        private void Heal(HealPotion healPotion)
        {
            bool pozioneUsata = false;

            if (_instanceData.Health + healPotion.HealPoint > _instanceData.MaxHealth)
            {
                _instanceData.Health = _instanceData.MaxHealth;
                pozioneUsata = true;

            }

            else
            {
                _instanceData.Health += healPotion.HealPoint;
                pozioneUsata = true;

            }

            if (pozioneUsata == true)
            {
                Destroy(healPotion.gameObject);
                RaiseHealChangedEvent();
            }

            //if (_instanceData.Health < _instanceData.MaxHealth)
            //    Destroy(healPotion.gameObject);

        }

        //private void OnTriggerStay2D(Collider2D collision)
        //{

        //    //if (collision.gameObject.tag == "interactable")
        //    //{

        //    //    if (Input.GetKeyDown(KeyCode.E))
        //    //        collision.gameObject.SendMessage("__INTERACT__");

        //    //}

        //    if (collision.gameObject.tag == "move-platform")
        //    {
        //        //m_platformVelocity = collision.rigidbody.velocity;
        //        //m_platformVelocity.y = 0;
        //    }


        //}

        public void OnCharacterJump()
        {
            //_audioSource.PlayOneShot();

        }

        public int GetLifes()
        {
            return _instanceData.Lifes;

        }

        public int GetMaxHealth()
        {
            return _instanceData.MaxHealth;
        }

        public int GetHealth()
        {
            return _instanceData.Health;
        }

        private void RaiseHealChangedEvent()
        {
            if (_heartChangedEvent != null)
                _heartChangedEvent.Invoke();
        }

        public void __TAKE_DAMAGE__(DamageInfo damageInfo)
        {
            if (damageInfo.InstaKill == true)
            {
                _instanceData.Health = 0;
            }

            else
            {
                _instanceData.Health -= damageInfo.DamagePoint;

            }

            RaiseHealChangedEvent();

            if (_instanceData.Health > 0)
                _character.GetHit();

            else
            {
                if (!_isDead)
                {
                    _character.Die();
                    _isDead = true;
                    _instanceData.Lifes -= 1;

                    if (_instanceData.Lifes > 0)
                    {
                        if (_lostLifeEvent != null)
                            _lostLifeEvent.Invoke();

                    }
                    else
                    {
                        if (_gameOverEvent != null)
                            _gameOverEvent.Invoke();

                    }

                }

            }

        }

        public void __RESPAWN__()
        {
            _character.Revive();
            Debug.Log("RESPAWN");
            _isDead = false;
            _instanceData.Health = _instanceData.MaxHealth;
            _character.Revive();

            RaiseHealChangedEvent();
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            GameObject collisionGameObject = collision.gameObject;

            if (collisionGameObject.tag == "pass-collider")
            {
                Physics2D.IgnoreCollision(collision.GetComponent<Collider2D>(), _bodyCollider);
            }

            if (collisionGameObject.tag == "interactable")
            {
                _interactable = collision.gameObject;
            }

            if (collisionGameObject.tag == "PowerUp")
            {
                HealPotion healPotion = collisionGameObject.GetComponent<HealPotion>();
                if (healPotion != null)
                {
                    Heal(healPotion);
                }

            }

            if (collisionGameObject.tag == "CheckPoint")
            {
                if (_checkPointEvent != null)
                    _checkPointEvent.Invoke(collisionGameObject);

            }

        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.gameObject.tag == "pass-collider")
            {
                Physics2D.IgnoreCollision(collision.GetComponent<Collider2D>(),
                gameObject.GetComponent<CircleCollider2D>(), false);
            }

            if (collision.gameObject.tag == "interactable")
            {
                _interactable = null;
            }
        }

        private void OnTriggerStay2D(Collider2D collision)
        {
            //if (collision.gameObject.tag == "interactable")
            //{
            //    if (Input.GetKeyDown(KeyCode.E))
            //        collision.gameObject.SendMessage("__INTERACT__");
            //}

            if (collision.gameObject.tag == "moving-platform")
            {

            }
        }

        private void OnCollisionStay2D(Collision2D collision)
        {
            //Debug.Log("COLLISION ENTER");
            //_platformCollision = collision;

            if (collision.gameObject.tag == "Moving_Platform")
            {

                m_platformVelocity = collision.rigidbody.velocity;
                m_platformVelocity.y = 0;

            }

        }

        private void OnCollisionExit2D(Collision2D collision)
        {
            //Debug.Log("COLLISION EXIT");
            _platformCollision = null;

        }

       
    }


}
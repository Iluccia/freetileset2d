﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MyFirst2DPlatform.Mechanics
{
    public class ArcherEnemi : MonoBehaviour
    {
        [SerializeField] GameObject _arrowPrefab;
        [SerializeField] Transform _fireHole;
        [SerializeField] GameObject targets;
        [SerializeField] int AttackPoints;

        [SerializeField] int _lifePoints;
        [SerializeField] bool FlipOnStart;
        [SerializeField] float LifePoints;

        [SerializeField] float FirePower = 500;
        [SerializeField] float _viewDistance = 10;
        [SerializeField] float _viewRadius = 1;
        [SerializeField] float _waitSecondsForNextAttack = 5;
        [SerializeField] private float _timeAfterDeath = 1;

        GameObject _target;
        Coroutine _currentRoutine;
        CircleCollider2D _bodyCollider;
        Animator _animator;
        Vector2 origin;

        private bool FlipOnStar;
        bool _doAttack = true;
        bool m_FacingRight = true;
        bool _isDead;

        bool isdead;

        private Animator m_Anima;

        private void Awake()
        {
            _animator = GetComponent<Animator>();
            _bodyCollider = GetComponent<CircleCollider2D>();
            _fireHole = transform.Find("firehole");
        }

        // Use this for initialization
        void Start()
        {
            //Debug.Log("lifepoints " + _lifePoints);
            if (FlipOnStar)
                Flip();

            _currentRoutine = StartCoroutine(IdleLogic());
        }

        // Update is called once per frame
        void Update()
        {

        }

        private void Flip()
        {
            // Switch the way the player is labelled as facing.
            m_FacingRight = !m_FacingRight;

            // Multiply the player's x local scale by -1.
            Vector3 theScale = transform.localScale;
            theScale.x *= -1;
            transform.localScale = theScale;

        }

        private void OnGUI()
        {

            if (GUILayout.Button("Hit"))
            {
                gameObject.SendMessage("__TAKEDAMAGE__");
            }
            if (GUILayout.Button("ATTACK"))
            {
                Attack();

            }

        }

        void Attack()
        {
            _animator.SetTrigger("attack");

        }

        public void __SHOOT__()
        {
            GameObject newArrow = Instantiate<GameObject>(_arrowPrefab, _fireHole.position, Quaternion.identity);
            Physics2D.IgnoreCollision(newArrow.GetComponent<Collider2D>(), _bodyCollider);

            float randomness = Random.Range(0.9f, 1.1f);

            newArrow.GetComponent<Rigidbody2D>().AddForce(_fireHole.up * (FirePower * randomness));

        }

        //public void __TAKE_DAMAGE__(DamageInfo damageInfo)
        //{
        //    LifePoints -= damageInfo.DamagePoint;

        //    if (LifePoints > 0)
        //        m_Anima.SetTrigger("hit");
        //    else
        //    {
        //        if (!isdead)
        //        {
        //            // StopCoroutine(_currentRoutine);
        //            m_Anima.SetTrigger("dead");
        //            isdead = true;
        //        }
        //    }
        //}

        public void __TAKE_DAMEGE__(DamageInfo damageInfo)
        {
            Debug.Log("OUCH!");

            _lifePoints -= damageInfo.DamagePoint;

            if (_lifePoints > 0)
                _animator.SetTrigger("hit");
            else
            {
                if (!_isDead)
                {
                    SwitchRoutine(DeathLogic());

                }
            }

        }

        bool CanSeePlayer(out List<GameObject> targets)
        {
            bool result = false;
            targets = null;

            Vector2 origin = transform.position;
            Vector2 direction = transform.position + ((m_FacingRight) ? transform.right : -transform.right) * _viewDistance;
            int HIT_ARRAY_LENGHT = 10;
            RaycastHit2D[] hits = new RaycastHit2D[HIT_ARRAY_LENGHT];
            int hitCount = Physics2D.CircleCastNonAlloc(origin, _viewRadius, direction, hits, _viewDistance);

            // per debaggare

            //sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            //sphere.transform.position = direction;
            //sphere.transform.localScale = new Vector3(_castRadius * 2, _castRadius * 2, _castRadius * 2);

            //Debug.DrawLine(origin, direction, Color.blue);
            //Debug.Log("TARGETS" + hitCount);

            //GameObject sphere = null;

            //public WaitForSeconds(float seconds);
            //Destroy(sphere);

            if (hitCount > 0)
            {
                targets = new List<GameObject>();

                for (int i = 0; i < hitCount; i++)
                {
                    RaycastHit2D rh = hits[i];
                    GameObject hitGo = rh.collider.gameObject;

                    if (hitGo.tag == "Player")
                    {
                        _target = hitGo;

                        targets.Add(hitGo);
                        //Debug.Log("@#!$%$=?!^");
                        result = true;
                    }

                }

            }

            return result;
        }

        IEnumerator IdleLogic()
        {
            List<GameObject> targets = null;

            while (true)
            {
                if (CanSeePlayer(out targets))
                    SwitchRoutine(AttackLogic());
                yield return new WaitForSeconds(1);
            }

            
        }

        IEnumerator AttackLogic()
        {
            _doAttack = true;

            while (_doAttack)
            {
                //orienta arciere nella posizione del giocatore
                if ((_target.transform.position.x < gameObject.transform.position.x && m_FacingRight)
                    ||
                    (_target.transform.position.x > gameObject.transform.position.x && !m_FacingRight))
                    Flip();

                Attack();

                yield return new WaitForSeconds(_waitSecondsForNextAttack);

            }

        }

        IEnumerator DeathLogic()
        {
            _animator.SetTrigger("dead");
            _isDead = true;
            _bodyCollider.enabled = false;

            yield return new WaitForSeconds(_timeAfterDeath);

            Destroy(gameObject);
        }

        void SwitchRoutine(IEnumerator routine)
        {
            if (_currentRoutine != null)
            {
                StopCoroutine(_currentRoutine);

            }
            _currentRoutine = StartCoroutine(routine);
        }

    }

}

using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace MyFirst2DPlatform.Mechanics
{
    [RequireComponent(typeof(MyPlatformerCharacter2D))]
    public class EnhancedPlatformer2DUserControl : MonoBehaviour
    {  
        // usedcontroll e la gestione dell'imput
        private MyPlatformerCharacter2D m_Character;

        //private Vector2 m_platformVelocity;

        private bool m_JumpDown;
        public bool IsJumpingDown { get { return m_JumpDown; } }

        private bool m_Jump;
        public bool IsJumpingUp { get { return m_Jump; } set { m_Jump = value; } }

        private bool m_Attack;
        public bool IsAttacking { get { return m_Attack; } }

        private float _horinzontal;
        public float Horizontal { get { return _horinzontal; } }

        private bool m_Crouch;
        public bool IsCrouch { get { return m_Crouch; } }

        Collision2D _platformCollision;
    

        private void Awake()
        {
            m_Character = GetComponent<MyPlatformerCharacter2D>();
        }


        private void Update()
        {
            if (!m_Jump)
            {
                m_Attack = CrossPlatformInputManager.GetButtonDown("Fire1");
                // Read the jump input in Update so button presses aren't missed.
                m_Jump = CrossPlatformInputManager.GetButtonDown("Jump");
                Debug.Log("JUMP " + m_Jump);
                float v = CrossPlatformInputManager.GetAxis("Vertical");

                if (m_Jump && v < 0)
                {

                    //Debug.Log("V" + v);
                    m_JumpDown = true;

                }

                else

                    m_JumpDown = false;

            }

            if (_platformCollision != null && m_JumpDown && _platformCollision.gameObject.tag == "pass-collider")
            {
                Physics2D.IgnoreCollision(_platformCollision.collider, gameObject.GetComponent<Collider2D>(), true);

            }

        }

        private void FixedUpdate()
        {
            // Read the inputs.
            bool crouch = Input.GetKey(KeyCode.LeftControl);
            _horinzontal = CrossPlatformInputManager.GetAxis("Horizontal");
            // Pass all parameters to the character control script.
            //m_Character.Move(h, crouch, m_Jump, m_JumpDown, m_Attack, m_platformVelocity);
            //m_Jump = false;
            //m_platformVelocity = Vector2.zero;

        }

        //private void OnCollisionStay2D(Collision2D collision)
        //{
        //    //Debug.Log("COLLISION ENTER");
        //    //_platformCollision = collision;

        //    if (collision.gameObject.tag == "interactable")
        //    {



        //    }

        //    if (collision.gameObject.tag == "Moving_Platform")
        //    {

        //        m_platformVelocity = collision.rigidbody.velocity;

        //    }

        //}

        //private void OnCollisionExit2D(Collision2D collision)
        //{
        //    //Debug.Log("COLLISION EXIT");
        //    _platformCollision = null;

        //}


    }
}
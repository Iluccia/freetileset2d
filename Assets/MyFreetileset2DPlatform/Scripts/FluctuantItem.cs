﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FluctuantItem: MonoBehaviour
{
    [SerializeField] float Displacement;
    Vector3 startPosition;
    [SerializeField] [Range(0, 10)] float speed;

    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        Vector3 position = startPosition;
        position.y += Mathf.Sin(Time.time * speed) * Displacement;
        transform.position = position;
	}
}

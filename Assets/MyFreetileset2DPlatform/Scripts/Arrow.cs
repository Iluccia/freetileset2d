﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace MyFirst2DPlatform.Mechanics
{
    public class Arrow : MonoBehaviour
    {
        [SerializeField] int _lifePoints;
        [SerializeField] int Damage = 1;
        [SerializeField] float DestroyTime = 0.5f;
        [SerializeField] int _DamageArrow =1;

        BoxCollider2D _collider;
        Rigidbody2D _rigidBody;

        private void Awake()
        {
            _collider = GetComponent<BoxCollider2D>();
            _rigidBody = GetComponent<Rigidbody2D>();

        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void FixedUpdate()
        {
            //#if UNUTY_EDITOR

            Vector3 start = transform.position;
            Vector3 end = new Vector3(_rigidBody.velocity.x, _rigidBody.velocity.y);

            //Debug.DrawLine(start, start + end);

            //# endif

            Vector2 v = _rigidBody.velocity;
            if (v.magnitude > 0)
            {
                float angle = Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
                transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

            }
        }
        void OnCollisionEnter2D(Collision2D collision)
        {
            GameObject collisionGameObject = collision.gameObject;

            if (collisionGameObject.tag == "Player")
            {
                DamageInfo damage = new DamageInfo();
                damage.DamagePoint = _DamageArrow;
                collisionGameObject.SendMessage("__TAKEDAMAGE__", damage);
                transform.parent = collisionGameObject.transform;
            }

            GoToSleep();
        }

        private void GoToSleep()
        {
            _rigidBody.Sleep();
            _rigidBody.gravityScale = 0;
            _collider.enabled = false;
            Destroy(gameObject, DestroyTime);

        }


    }
}
